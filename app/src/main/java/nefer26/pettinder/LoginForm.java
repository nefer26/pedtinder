package nefer26.pettinder;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by ceset on 17.11.2018.
 */

public class LoginForm extends AppCompatActivity {
    private FirebaseAuth mAuth;
    Button btnFacebbok;
    Button btnTwitter;
    Button btnGiris;
    Button btnKayitOl;
    EditText txtEmail;
    EditText txtSifre;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_form);
        mAuth = FirebaseAuth.getInstance();
        componentCreate();
        btnClickListener();
    }

    public void componentCreate() {
        btnFacebbok = (Button) findViewById(R.id.loginBtnFacebook);
        btnTwitter = (Button) findViewById(R.id.loginBtnTwitter);
        btnKayitOl = (Button) findViewById(R.id.loginBtnKayitOl);
        btnGiris = (Button) findViewById(R.id.loginBtnGiris);
        txtEmail = (EditText) findViewById(R.id.loginTxtEmail);
        txtSifre = (EditText) findViewById(R.id.loginTxtPassword);
    }

    public void btnClickListener() {
        btnKayitOl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), UyeOlForm.class);
                startActivity(intent);
            }
        });

        btnGiris.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Task<AuthResult> authResultTask = mAuth.signInWithEmailAndPassword(txtEmail.getText().toString(), txtSifre.getText().toString()).addOnCompleteListener(LoginForm.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("", "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Intent intent = new Intent(getApplicationContext(), ListeleForm.class);
                            startActivity(intent);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("", "signInWithEmail:failure", task.getException());
                            Toast.makeText(LoginForm.this, "Giriş Başarızı.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            Intent intent = new Intent(getApplicationContext(), ListeleForm.class);
            startActivity(intent);
            finish();
        }
    }

    private void getCurrentUser() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            // Name, email address, and profile photo Url
            String name = user.getDisplayName();
            String email = user.getEmail();
            // Uri photoUrl = user.getPhotoUrl();

            // Check if user's email is verified
            boolean emailVerified = user.isEmailVerified();

            // The user's ID, unique to the Firebase project. Do NOT use this value to
            // authenticate with your backend server, if you have one. Use
            // FirebaseUser.getIdToken() instead.
            String uid = user.getUid();
        }
    }
}
