package nefer26.pettinder;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import entity.Hayvanlar;

/**
 * Created by ceset on 17.11.2018.
 */

public class HayvanEkleForm extends AppCompatActivity {
    Button btnFotoEkle;
    Button btnHayvanKaydet;
    EditText txtHayvanAdi;
    EditText txtHayvanIrk;
    EditText txtHayvanCinsiyet;
    EditText txtHayvanYas;
    EditText txtHayvanTur;
    EditText txtHayvanRenk;
    private final int PICK_IMAGE_REQUEST = 71;
    private Uri filePath;

    private ArrayList<Hayvanlar> hayvanlarList = new ArrayList<>();
    private Hayvanlar hayvanlar = new Hayvanlar();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hayvan_ekle);
        componentCreate();
        btnClick();
        initDatabase();
    }

    public void initDatabase() {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference("users");

        DatabaseReference productsRef = databaseReference.child(user.getUid());


        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Hayvanlar hayvanlar = ds.getValue(Hayvanlar.class);
                    hayvanlarList.add(hayvanlar);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        productsRef.addListenerForSingleValueEvent(eventListener);
    }

    public void componentCreate() {
        btnFotoEkle = (Button) findViewById(R.id.hayvanEkleBtnFotoSec);
        btnHayvanKaydet = (Button) findViewById(R.id.hayvanEkleBtnKaydet);

        txtHayvanAdi = (EditText) findViewById(R.id.hayvanAdiId);
        txtHayvanIrk = (EditText) findViewById(R.id.hayvanIrkiId);
        txtHayvanCinsiyet = (EditText) findViewById(R.id.hayvanCinsiyetId);
        txtHayvanYas = (EditText) findViewById(R.id.hayvanYasId);
        txtHayvanTur = (EditText) findViewById(R.id.hayvanTuruId);
        txtHayvanRenk = (EditText) findViewById(R.id.hayvanRenkId);


    }

    public void btnClick() {
        btnFotoEkle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImage();
            }
        });
        btnHayvanKaydet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                kaydet();
            }
        });
    }

    //Telefondan resim seçmeyi sağlayan metod
    private void chooseImage() {

       /* Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, );*/

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    //Hayvanını Kaydeder
    private void kaydet() {
        if(txtHayvanAdi.getText().toString().equals("")){
            Toast.makeText(HayvanEkleForm.this, "Adını Giriniz.",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        if(txtHayvanTur.getText().toString().equals("")){
            Toast.makeText(HayvanEkleForm.this, "Türünü Giriniz.",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        if(txtHayvanIrk.getText().toString().equals("")){
            Toast.makeText(HayvanEkleForm.this, "Irkını Giriniz.",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        if(txtHayvanCinsiyet.getText().toString().equals("")){
            Toast.makeText(HayvanEkleForm.this, "Cinsiyet Giriniz.",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        if(txtHayvanYas.getText().toString().equals("")){
            try {
                Integer.valueOf(txtHayvanYas.getText().toString());
            }catch (Exception e){
                Toast.makeText(HayvanEkleForm.this, "Yaş'ı Sayısısal Değer Giriniz.",
                        Toast.LENGTH_SHORT).show();
            }
            Toast.makeText(HayvanEkleForm.this, "Yaş Giriniz.",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        if(txtHayvanRenk.getText().toString().equals("")){
            Toast.makeText(HayvanEkleForm.this, "Renk Giriniz.",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference("users");
        hayvanlar.setId(UUID.randomUUID().toString());
        hayvanlar.setHayvanSahipId(user.getUid());
        hayvanlar.setHayvanAdi(txtHayvanAdi.getText().toString());
        hayvanlar.setHayvanIrk(txtHayvanIrk.getText().toString());
        hayvanlar.setHayvanCinsiyet(txtHayvanCinsiyet.getText().toString());
        hayvanlar.setHayvanYas(txtHayvanYas.getText().toString());
        hayvanlar.setHayvanTur(txtHayvanTur.getText().toString());
        hayvanlar.setHayvanRenk(txtHayvanRenk.getText().toString());

        hayvanlarList.add(hayvanlar);
        databaseReference.child(user.getUid()).setValue(hayvanlarList);
        onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                //   imageView.setImageBitmap(bitmap);
                Bitmap resizedBitmap = Bitmap.createScaledBitmap(
                        bitmap, 200, 300, false);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                resizedBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

                hayvanlar.setResim(encoded);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}