package nefer26.pettinder;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import entity.Kullanicilar;

/**
 * Created by ceset on 17.11.2018.
 */

public class UyeOlForm extends AppCompatActivity {
    private FirebaseAuth mAuth;

    EditText txtEmail;
    EditText txtSifre;
    EditText txtSifreRepeat;
    Button btnKayitOl;
    EditText txtTelefon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uye_ol_form);
        mAuth = FirebaseAuth.getInstance();
        componentCreate();
        btnClickListener();
    }

    public void componentCreate() {
        txtEmail = (EditText) findViewById(R.id.uyeOlTxtEmail);
        txtSifre = (EditText) findViewById(R.id.uyeOlTxtPassword);
        txtSifreRepeat = (EditText) findViewById(R.id.uyeOlTxtPasswordRepeat);
        btnKayitOl = (Button) findViewById(R.id.uyeOlBtnKayit);
        txtTelefon = (EditText) findViewById(R.id.uyeOlTxtTelefon);
    }

    public void btnClickListener() {
        btnKayitOl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtEmail.getText().toString() == null || txtEmail.getText().toString().equals("")) {
                    Toast.makeText(UyeOlForm.this, "Lütfen E-mail Giriniz",
                            Toast.LENGTH_SHORT).show();
                } else if (txtSifre.getText() == null || txtSifre.getText().toString().equals("") || txtSifreRepeat.getText() == null || txtSifreRepeat.getText().toString().equals("")) {
                    Toast.makeText(UyeOlForm.this, "Lütfen Şifrenizi Giriniz",
                            Toast.LENGTH_SHORT).show();
                } else if (!txtSifre.getText().toString().equals(txtSifreRepeat.getText().toString())) {
                    Toast.makeText(UyeOlForm.this, "Şifrenizi Kontrol Ediniz",
                            Toast.LENGTH_SHORT).show();
                } else if (txtSifre.getText().toString().length() < 6) {
                    Toast.makeText(UyeOlForm.this, "Şifreniz En az 6 Karakter Olmalıdır",
                            Toast.LENGTH_SHORT).show();
                } else if (txtTelefon.getText() == null || txtTelefon.getText().toString().equals("")) {
                    Toast.makeText(UyeOlForm.this, "Lütfen Telefon Numaranızı Giriniz",
                            Toast.LENGTH_SHORT).show();
                } else {
                    mAuth.createUserWithEmailAndPassword(txtEmail.getText().toString(), txtSifre.getText().toString())
                            .addOnCompleteListener(UyeOlForm.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {

                                        FirebaseUser user = mAuth.getCurrentUser();
                                        Kullanicilar kullanicilar = new Kullanicilar();
                                        kullanicilar.setId(user.getUid());
                                        kullanicilar.setTelNo(txtTelefon.getText().toString());
                                        kullanicilar.setEmail(user.getEmail());
                                        FirebaseDatabase.getInstance().getReference("kullanicilar").child(user.getUid()).setValue(kullanicilar)
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {
                                                        Log.d("xxxxx", "yyyy");
                                                    }
                                                })
                                                .addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        Log.d("xxxxxx", "zzzzzzz");
                                                    }
                                                });

                                        Log.d("", "createUserWithEmail:success");
                                        //FirebaseUser user = mAuth.getCurrentUser();
                                        Intent intent = new Intent(getApplicationContext(), ListeleForm.class);
                                        startActivity(intent);
                                        finish();
                                    } else {
                                        // If sign in fails, display a message to the user.
                                        Log.w("", "createUserWithEmail:failure", task.getException());
                                        Toast.makeText(UyeOlForm.this, "Kayıt Başarız.",
                                                Toast.LENGTH_SHORT).show();
                                    }

                                    // ...
                                }
                            });
                }

            }
        });
    }
}
