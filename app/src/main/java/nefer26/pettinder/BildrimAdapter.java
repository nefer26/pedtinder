package nefer26.pettinder;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import entity.Hayvanlar;
import entity.Kullanicilar;

/**
 * Created by ceset on 18.11.2018.
 */

public class BildrimAdapter extends BaseAdapter {
    private LayoutInflater urunInflater;
    private HashMap<String, String> list;
    private Activity activity;

    public BildrimAdapter(Activity activity, HashMap<String, String> list) {
        urunInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = list;
        this.activity = activity;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        View lineView;
        lineView = urunInflater.inflate(R.layout.bildirimadapter, null);
        final TextView txtAdapter = (TextView) lineView.findViewById(R.id.adaptertxtId);
        final ImageButton imageButton = (ImageButton) lineView.findViewById(R.id.adapterImageId);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final List<Hayvanlar> listHayvan = new ArrayList<>();

        FirebaseDatabase.getInstance().getReference("kullanicilar").child(new ArrayList<Map.Entry<String, String>>(list.entrySet()).get(position).getKey().split("\\?")[0]).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final Kullanicilar kullanicilar = dataSnapshot.getValue(Kullanicilar.class);
                txtAdapter.setText(kullanicilar.getEmail());
                txtAdapter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                        builder.setMessage("İletişime Geç!")
                                .setCancelable(false)
                                .setPositiveButton("Kapat", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                })
                                .setNegativeButton("Ara", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + kullanicilar.getTelNo()));
                                        if (ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                            // TODO: Consider calling
                                            //    ActivityCompat#requestPermissions
                                            // here to request the missing permissions, and then overriding
                                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                            //                                          int[] grantResults)
                                            // to handle the case where the user grants the permission. See the documentation
                                            // for ActivityCompat#requestPermissions for more details.
                                            return;
                                        }
                                        activity.startActivity(intent);
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        DatabaseReference  mDatabase = database.getReference("users");
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listHayvan.clear();
                dataSnapshot.getChildren();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    for (DataSnapshot dgs : ds.getChildren()) {
                        Hayvanlar hvyn = ds.child(dgs.getKey().toString()).getValue(Hayvanlar.class);
                        if (hvyn.getHayvanSahipId().equals(new ArrayList<Map.Entry<String, String>>(list.entrySet()).get(position).getValue().toString()) && hvyn.getId().equals(new ArrayList<Map.Entry<String, String>>(list.entrySet()).get(position).getKey().split("\\?")[1])) {

                            if (hvyn != null && hvyn.getResim() != null) {
                                byte[] decodedString = Base64.decode(hvyn.getResim(), Base64.DEFAULT);
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                BitmapDrawable bitmapDrawable = new BitmapDrawable(decodedByte);
                                imageButton.setBackground(bitmapDrawable);
                            }
                        }
                    }
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



      /*
        txtUrunAdi.setText(String.valueOf(siparisIcerik.getUrunAdi()));
        txtUrunAdeti.setText(String.valueOf(siparisIcerik.getAdet()));
        Button btn= (Button) lineView.findViewById(R.id.buttonSiparisArtir);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.get(position).setAdet(siparisIcerik.getAdet()+1);
                txtUrunAdeti.setText(String.valueOf(siparisIcerik.getAdet()+1));
            }


        });
*/
        return lineView;
    }

    public void updateResult(HashMap<String, String> list) {
        this.list = list;
        notifyDataSetChanged();
    }


    public HashMap<String, String> getList() {
        return this.list;
    }


}