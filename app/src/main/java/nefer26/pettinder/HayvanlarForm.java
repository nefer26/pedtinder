package nefer26.pettinder;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import entity.Hayvanlar;

/**
 * Created by ceset on 18.11.2018.
 */

public class HayvanlarForm extends AppCompatActivity {
    ListView lst;
    private DatabaseReference mDatabase;
    private final FirebaseDatabase database = FirebaseDatabase.getInstance();
    HayvanlarAdapter hayvanlarAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hayvan_form);
        componentCreate();
        hayvanlarAdapter = new HayvanlarAdapter(this, new ArrayList<Hayvanlar>());
        lst.setAdapter(hayvanlarAdapter);
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseDatabase.getInstance().getReference("users").child(user.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final List<Hayvanlar> listHayvan = new ArrayList<>();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Hayvanlar hayvanlar = ds.getValue(Hayvanlar.class);
                    listHayvan.add(hayvanlar);
                }
                hayvanlarAdapter.updateResult(listHayvan);
                Log.d("asadfsdfsdfsdfd", String.valueOf(listHayvan.size()));

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void componentCreate() {
        lst = (ListView) findViewById(R.id.lstBildirimHayvan);
    }
}
