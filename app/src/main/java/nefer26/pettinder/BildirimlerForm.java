package nefer26.pettinder;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import entity.Hayvanlar;

/**
 * Created by ceset on 18.11.2018.
 */

public class BildirimlerForm extends AppCompatActivity {
    ListView lst;
    private DatabaseReference mDatabase;
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    final List<Hayvanlar> listHayvan = new ArrayList<>();
    HashMap<String,String> list=new HashMap<>();
    BildrimAdapter bildirimAdapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bildirimler_form);
        componentCreate();
            bildirimAdapter=new BildrimAdapter(this,list);
            lst.setAdapter(bildirimAdapter);
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        mDatabase = database.getReference("bildirimler");
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listHayvan.clear();
                dataSnapshot.getChildren();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                   if(ds.getValue().equals(user.getUid())){
                       list.put(ds.getKey().toString(),ds.getValue().toString());
                   }
                }
                bildirimAdapter.updateResult(list);
                Log.d("asadfsdfsdfsdfd", String.valueOf(listHayvan.size()));

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void componentCreate(){
        lst= (ListView) findViewById(R.id.lstBildirim);
    }
}
