package nefer26.pettinder;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import entity.Hayvanlar;

/**
 * Created by ceset on 18.11.2018.
 */

public class HayvanlarAdapter extends BaseAdapter {
    private LayoutInflater urunInflater;
    private List<Hayvanlar> list;


    public HayvanlarAdapter(Activity activity, List<Hayvanlar> list) {
        urunInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        View lineView;
        lineView = urunInflater.inflate(R.layout.hayvandapter, null);
        final TextView txtAdapter = (TextView) lineView.findViewById(R.id.adaptertxtIdHayvan);
        final ImageButton imageButton = (ImageButton) lineView.findViewById(R.id.adapterImageIdHayvan);

        txtAdapter.setText(list.get(position) == null ? "" : list.get(position).getHayvanAdi() == null ? "" : list.get(position).getHayvanAdi());

        if (list.get(position) != null && list.get(position).getResim() != null) {
            byte[] decodedString = Base64.decode(list.get(position).getResim(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            BitmapDrawable bitmapDrawable = new BitmapDrawable(decodedByte);
            imageButton.setBackground(bitmapDrawable);
        }

        return lineView;
    }

    public void updateResult(List<Hayvanlar> list) {
        this.list = list;
        notifyDataSetChanged();
    }


    public List<Hayvanlar> getList() {
        return this.list;
    }


}