package nefer26.pettinder;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Comment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import entity.Bildirim;
import entity.Hayvanlar;

/**
 * Created by ceset on 17.11.2018.
 */

public class ListeleForm extends AppCompatActivity {


    private DrawerLayout mDrawerLayout;
    FloatingActionButton fac;
    private DatabaseReference mDatabase;
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    final List<Hayvanlar> listHayvan = new ArrayList<>();
    List<Hayvanlar> lstHayvanFind = new ArrayList<Hayvanlar>();
    Button btnKopek;
    Button btnKedi;
    Button btnMaymun;
    Button btnInek;
    Button btnAt;
    Button btnBalik;
    GridLayout lstGrid;
    EditText txtAra;
    Button btnFilter;
    Button last;
    HashMap<String, String> bildirimHast = new HashMap();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listele_form);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.drawer_menu);


        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        if (menuItem.getTitle().equals("Çıkış")) {
                            FirebaseAuth.getInstance().signOut();
                            Intent intent = new Intent(getApplicationContext(), LoginForm.class);
                            startActivity(intent);
                            finish();
                        } else if (menuItem.getTitle().equals("Bildirimlerim")) {

                            Intent intent = new Intent(getApplicationContext(), BildirimlerForm.class);
                            startActivity(intent);

                        } else if (menuItem.getTitle().equals("Hayvanlarım")) {

                            Intent intent = new Intent(getApplicationContext(), HayvanlarForm.class);
                            startActivity(intent);

                        }

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        return true;
                    }
                });
        componentCreate();
        btnClickListener();
        txtListener();

        mDatabase = database.getReference("users");
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listHayvan.clear();
                dataSnapshot.getChildren();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    for (DataSnapshot dgs : ds.getChildren()) {
                        listHayvan.add(ds.child(dgs.getKey().toString()).getValue(Hayvanlar.class));
                    }
                }
                Log.d("asadfsdfsdfsdfd", String.valueOf(listHayvan.size()));
                if (last != null) {
                    last.callOnClick();
                } else
                    listeDoldur(listHayvan);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void componentCreate() {
        fac = (FloatingActionButton) findViewById(R.id.fab);
        btnAt = (Button) findViewById(R.id.lstBtnAt);
        btnBalik = (Button) findViewById(R.id.lstBtnBalik);
        btnInek = (Button) findViewById(R.id.lstBtnInek);
        btnKedi = (Button) findViewById(R.id.lstBtnKedi);
        btnKopek = (Button) findViewById(R.id.lsteBtnKopek);
        btnMaymun = (Button) findViewById(R.id.lstBtnMaymun);
        lstGrid = (GridLayout) findViewById(R.id.lstGrid);

        txtAra = (EditText) findViewById(R.id.lstTxtAra);
        btnFilter= (Button) findViewById(R.id.lsteBtnFilter);
    }

    public void btnClickListener() {
        fac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), HayvanEkleForm.class);
                startActivity(intent);
            }
        });
        btnAt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lstHayvanFind.clear();
                for (Hayvanlar hayvan : listHayvan) {
                    if ("At".equals(hayvan.getHayvanTur())) {
                        lstHayvanFind.add(hayvan);
                    }
                }
                listeDoldur(lstHayvanFind);
                last = btnAt;
            }
        });
        btnMaymun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lstHayvanFind.clear();
                for (Hayvanlar hayvan : listHayvan) {
                    if ("Maymun".equals(hayvan.getHayvanTur())) {
                        lstHayvanFind.add(hayvan);
                    }
                }
                listeDoldur(lstHayvanFind);
                last = btnMaymun;
            }
        });
        btnKopek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lstHayvanFind.clear();
                for (Hayvanlar hayvan : listHayvan) {
                    if ("Köpek".equals(hayvan.getHayvanTur())) {
                        lstHayvanFind.add(hayvan);
                    }
                }
                listeDoldur(lstHayvanFind);
                last = btnKopek;
            }
        });
        btnKedi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lstHayvanFind.clear();
                for (Hayvanlar hayvan : listHayvan) {
                    if ("Kedi".equals(hayvan.getHayvanTur())) {
                        lstHayvanFind.add(hayvan);
                    }
                }
                listeDoldur(lstHayvanFind);
                last = btnKedi;
            }
        });
        btnBalik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lstHayvanFind.clear();
                for (Hayvanlar hayvan : listHayvan) {
                    if ("Balık".equals(hayvan.getHayvanTur())) {
                        lstHayvanFind.add(hayvan);
                    }
                }
                listeDoldur(lstHayvanFind);
                last = btnBalik;
            }
        });
        btnInek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lstHayvanFind.clear();
                for (Hayvanlar hayvan : listHayvan) {
                    if ("İnek".equals(hayvan.getHayvanTur())) {
                        lstHayvanFind.add(hayvan);
                    }
                }
                listeDoldur(lstHayvanFind);
                last = btnInek;
            }
        });
        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ListeleForm.this);
                builder.setMessage("Arama")
                        .setCancelable(false)
                        .setPositiveButton("Kapat", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Getir", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

    }

    public void txtListener() {
        txtAra.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                lstHayvanFind.clear();
                for (Hayvanlar hayvan : listHayvan) {
                    if (hayvan.getHayvanTur() != null && hayvan.getHayvanTur().toUpperCase().contains(txtAra.getText().toString().toUpperCase())) {
                        lstHayvanFind.add(hayvan);
                    }
                }
                if (charSequence.length() != 0) {
                    listeDoldur(lstHayvanFind);
                } else {
                    listeDoldur(listHayvan);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void listeDoldur(final List<Hayvanlar> lst) {
        lstGrid.removeAllViews();
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(getWindowManager().getDefaultDisplay().getWidth() / 2, getWindowManager().getDefaultDisplay().getHeight() / 4);

        lp.addRule(RelativeLayout.CENTER_IN_PARENT, 1);

        ImageButton[] btnGreen = new ImageButton[lst.size()];
        for (int i = 0; i < lst.size(); i++) {
            final int x = i;
            btnGreen[i] = new ImageButton(this);

            btnGreen[i].setLayoutParams(lp);
            if (lst.get(i) != null && lst.get(i).getResim() != null) {
                byte[] decodedString = Base64.decode(lst.get(i).getResim(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), decodedByte);
                btnGreen[i].setBackground(bitmapDrawable);
            }
            //   btnGreen[i].setOnClickListener(ClickListener);
            //  btnGreen[i].setBackgroundColor(Color.TRANSPARENT);
            btnGreen[i].setTag(i);
            btnGreen[i].setId(i);

            final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = database.getReference("bildirimler");

            DatabaseReference productsRef = databaseReference.child(lst.get(x).getHayvanSahipId());

            final Set<Bildirim> bildirimList = new HashSet<>();

            ChildEventListener childEventListener = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                    Log.d("", "onChildAdded:" + dataSnapshot.getKey());

                    // A new comment has been added, add it to the displayed list
                   /* Bildirim comment = dataSnapshot.getValue(Bildirim.class);
                    bildirimList.add(comment);
                    Log.d("x","x");
                    */
                    // ...
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                    Log.d("", "onChildChanged:" + dataSnapshot.getKey());

                    // A comment has changed, use the key to determine if we are displaying this
                    // comment and if so displayed the changed comment.


                    // ...
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    Log.d("", "onChildRemoved:" + dataSnapshot.getKey());

                    // A comment has changed, use the key to determine if we are displaying this
                    // comment and if so remove it.


                    // ...
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
                    Log.d("", "onChildMoved:" + dataSnapshot.getKey());

                    // A comment has changed position, use the key to determine if we are
                    // displaying this comment and if so move it.
                    Comment movedComment = dataSnapshot.getValue(Comment.class);
                    String commentKey = dataSnapshot.getKey();

                    // ...
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.w("", "postComments:onCancelled", databaseError.toException());
                   /* Toast.makeText(mContext, "Failed to load comments.",
                            Toast.LENGTH_SHORT).show();*/
                }
            };
            productsRef.addChildEventListener(childEventListener);
           /* ValueEventListener eventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    bildirimList.clear();
                    /*for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        Bildirim bildirim = ds.getValue(Bildirim.class);
                        bildirimList.add(bildirim);
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };
            productsRef.addListenerForSingleValueEvent(eventListener);*/

            btnGreen[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ListeleForm.this);
                    builder.setMessage("İletişime Geç!")
                            .setCancelable(false)
                            .setPositiveButton("Kapat", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            })
                            .setNegativeButton("Gönder", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    FirebaseDatabase database2 = FirebaseDatabase.getInstance();
                                    DatabaseReference databaseReference2 = database2.getReference("bildirimler");
                                    Bildirim bildirim = new Bildirim();
                                    bildirim.setGonderenId(FirebaseAuth.getInstance().getCurrentUser().getUid());
                                    bildirim.setHayvanId(lst.get(x).getId());
                                    bildirimList.add(bildirim);
                                    ArrayList<Bildirim> a = (ArrayList<Bildirim>) new ArrayList<>(bildirimList).clone();
                                    bildirimList.clear();
                                    //databaseReference2.child(lst.get(x).getHayvanSahipId()).setValue(a);
                                    //  HashMap<String,String> bildirimHast=new HashMap();
                                    //bildirimHast.clear();
                                    //bildirimHast.put(FirebaseAuth.getInstance().getCurrentUser().getUid()+"?"+lst.get(x).getId(),lst.get(x).getHayvanSahipId());
                                    databaseReference2.child(FirebaseAuth.getInstance().getCurrentUser().getUid() + "?" + lst.get(x).getId()).setValue(lst.get(x).getHayvanSahipId())
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    // Write was successful!
                                                    Log.d("xxxxx", "yyyy");
                                                    // ...
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Log.d("xxxxxx", "zzzzzzz");
                                                }
                                            });
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });

            lstGrid.addView(btnGreen[i]);
        }
    }

    public void initDatabase() {


    }
}
