package entity;

/**
 * Created by ceset on 18.11.2018.
 */

public class Bildirim {

    private String hayvanId;
    private String gonderenId;


    public String getHayvanId() {
        return hayvanId;
    }

    public void setHayvanId(String hayvanId) {
        this.hayvanId = hayvanId;
    }

    public String getGonderenId() {
        return gonderenId;
    }

    public void setGonderenId(String gonderenId) {
        this.gonderenId = gonderenId;
    }

    @Override
    public int hashCode() {
        return hayvanId.hashCode() + gonderenId.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        Bildirim x = (Bildirim) obj;
        if(x.getHayvanId().equals(hayvanId) && x.getGonderenId().equals(getGonderenId()))
            return true;
        return false;
    }
}
