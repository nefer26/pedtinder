package entity;

import java.util.Date;

/**
 * Created by ceset on 18.11.2018.
 */

public class Hayvanlar {
    String id;
    String hayvanAdi;
    String hayvanIrk;
    String hayvanCinsiyet;
    String hayvanYas;
    String hayvanTur;
    String hayvanRenk;
    String fotoId;
    String hayvanSahipId;
    Date eklenmeTarihi;
    String resim;
    boolean aktif;


    public String getHayvanSahipId() {
        return hayvanSahipId;
    }

    public void setHayvanSahipId(String hayvanSahipId) {
        this.hayvanSahipId = hayvanSahipId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFotoId() {
        return fotoId;
    }

    public void setFotoId(String fotoId) {
        this.fotoId = fotoId;
    }

    public boolean isAktif() {
        return aktif;
    }

    public void setAktif(boolean aktif) {
        this.aktif = aktif;
    }

    public Date getEklenmeTarihi() {
        return eklenmeTarihi;
    }

    public void setEklenmeTarihi(Date eklenmeTarihi) {
        this.eklenmeTarihi = eklenmeTarihi;
    }

    public String getResim() {
        return resim;
    }

    public void setResim(String resim) {
        this.resim = resim;
    }


    public String getHayvanAdi() {
        return hayvanAdi;
    }

    public void setHayvanAdi(String hayvanAdi) {
        this.hayvanAdi = hayvanAdi;
    }

    public String getHayvanIrk() {
        return hayvanIrk;
    }

    public void setHayvanIrk(String hayvanIrk) {
        this.hayvanIrk = hayvanIrk;
    }

    public String getHayvanCinsiyet() {
        return hayvanCinsiyet;
    }

    public void setHayvanCinsiyet(String hayvanCinsiyet) {
        this.hayvanCinsiyet = hayvanCinsiyet;
    }

    public String getHayvanYas() {
        return hayvanYas;
    }

    public void setHayvanYas(String hayvanYas) {
        this.hayvanYas = hayvanYas;
    }

    public String getHayvanTur() {
        return hayvanTur;
    }

    public void setHayvanTur(String hayvanTur) {
        this.hayvanTur = hayvanTur;
    }

    public String getHayvanRenk() {
        return hayvanRenk;
    }

    public void setHayvanRenk(String hayvanRenk) {
        this.hayvanRenk = hayvanRenk;
    }
}
